#!/bin/bash

sudo sh -c 'apt-get update -y'
sudo sh -c 'apt-get install uild-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev -y'
sudo sh -c 'apt-get install libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev -y'
sudo sh -c 'apt-get install python-rpi.gpio -y'

wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz
tar xf Python-3.7.0.tar.xz
sh -c ./Python-3.7.0/configure
make -j 4
sudo sh -c 'make altinstall'
rm Python-3.7.0.tar.xz
sudo sh -c 'apt-get autoremove -y'
sudo sh -c 'apt-get clean'
sudo sh -c 'pip3 install --upgrade pip'